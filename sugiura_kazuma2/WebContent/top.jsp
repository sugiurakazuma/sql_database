<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">


			<c:if test="${ empty loginUser }">
				<a href="login">あ</a>
			</c:if>

			<c:if test="${ not empty loginUser }">

				<a href="managment">ユーザー管理</a>
				<a href="new_contribution.jsp">新規投稿</a>
				<a href="logout">ログアウト</a>
				<div class="name">
					<c:out value="ようこそ${loginUser.name }さん" />
					<form action="./" method="get">
						<input type="date" name="start"> <input type="date"
							name="end"> <input type="submit" value="検索" /><br /> <input
							name="inputCategory" value="${inputCategory }" id="inputCategory" />
						<input type="submit" value="検索">
					</form>
				</div>


				<div class="messages">
					<c:forEach var="message" items="${messages }">
						<div class="message">
							<div class="name">
								<span class="name"><c:out value="${message.name}さんの投稿" /></span>
							</div>
							<c:out value="${message.id }" />
							<div class="subject">
								<c:out value="件名:${message.subject }" />
							</div>
							<div class="category">
								<c:out value="カテゴリー:${message.category }" />
							</div>
							<div class="text">
								<c:out value="本文:${message.text }" />
							</div>
							<div class="date">
								<fmt:formatDate value="${message.created_at}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<c:if test="${message.user_id == loginUser.id}" var="flg" />
							<c:if test="${flg}">
								<form action="delete" method="post">
									<input name="message_id" value="${message.id }" id="message_id"
										type="hidden" /> <input type="submit" value="削除">

								</form>
							</c:if>
							<c:forEach var="comment" items="${comments }">
								<c:if test="${comment.message_id == message.id}" var="flg2" />
								<c:if test="${flg2 }">
									<c:out value="${message.name }さんのコメント" />
									<div class="text">
										<c:out value="本文:${comment.text }" />
									</div>
									<div class="date">
										<fmt:formatDate value="${comment.created_at}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<c:if test="${comment.user_id == loginUser.id}" var="flg" />
									<c:if test="${flg}">
										<form action="delete" method="post">
											<input name="message_id" value="${message.id }"
												id="message_id" type="hidden" /> <input type="submit"
												value="削除">


										</form>
									</c:if>
								</c:if>
							</c:forEach>




							<form action="new_comment" method="post">
								<textarea name="text" cols="50" rows="5" class="text-box"></textarea>
								<br /> <input name="message_id" value="${message.id }"
									id="message_id" type="hidden" /> <input name="login_id"
									value="${loginUser.id }" id="login_id" type="hidden" /> <input
									type="submit" value="コメントする">
							</form>

						</div>
					</c:forEach>

				</div>

			</c:if>

		</div>
	</div>
</body>
</html>