package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserListService;

@WebServlet(urlPatterns = { "/managment" })
public class ManagmentTopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();

		User user = (User) request.getSession().getAttribute("loginUser");

		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		List<User> UserList = new UserListService().getUserList();

		session.setAttribute("users", UserList);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("managment.jsp").forward(request, response);
	}
}
