package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class Subject_MessageDao {
	public void insert(Connection connection, Message message) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions(");
			sql.append("id");
			sql.append(",subject");
			sql.append(",text");
			sql.append(",categry");
			sql.append(",created_at");
			sql.append(",updated_at");
			sql.append(")VALUES(");
			sql.append("NEXT VALUE FOR my_sql");
			sql.append(",?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getId());
			ps.setString(2, message.getSubject());
			ps.setString(3, message.getText());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private void close(PreparedStatement ps) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
