package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import beans.Users;

public class UserListServlet extends HttpServlet {
	@Resource(name = "jdbc/test")
	private DataSource ds;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Connection connection = null;

		try {
			connection = ds.getConnection();
			List<Users> list = new ArrayList<Users>();
			Statement stmt = connection.createStatement();
			String sql = "SELECT * from users";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				String login_id = rs.getString("login_id");
				String branch_id = rs.getString("branch_id");
				String position_id = rs.getString("position_id");
				list.add(new Users(id, name, login_id, branch_id, position_id));

			}

			req.setAttribute("Users", list);
			getServletContext().getRequestDispatcher("managment.jsp").forward(req, res);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				throw new ServletException();
			}
		}
	}
}