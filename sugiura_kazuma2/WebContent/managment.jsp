<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="./">戻るよ</a> <a href="signup">ユーザー新規登録</a><br />


			<c:out value="id" />
			<c:out value="名前" />
			<c:out value="ログインid" />
			<c:out value="支店" />
			<c:out value="役職" />
			<br />

			<c:forEach var="users" items="${ users }">


				<c:out value="${users.id }" />
				<c:out value="${users.name }" />
				<c:out value="${users.login_id }" />
				<c:out value="${users.branch_id }" />
				<c:out value="${users.position_id }" />
				<c:out value="${users.is_deleted }" />
				<form action="is_deliteds" method="post">
					<input type="submit" value="停止/再開" />
				</form>
				<form action="settings" method="post">
					<input type="submit" value="編集" />
				</form>

			</c:forEach>

		</div>
	</div>
</body>
</html>
