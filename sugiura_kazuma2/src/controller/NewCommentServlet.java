package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.NewComment;
import service.NewCommentService;

@WebServlet(urlPatterns = { "/new_comment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<String> comments = new ArrayList<String>();
		HttpSession session = request.getSession();
		if (isValid(request, comments) == true) {

			NewComment comment = new NewComment();

			comment.setText(request.getParameter("text"));
			comment.setUser_id(Integer.parseInt(request.getParameter("login_id")));
			comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));


			new NewCommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String text = request.getParameter("text");

		if (StringUtils.isEmpty(text) == true) {
			comments.add("コメントを入力してください");
		}

		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}
