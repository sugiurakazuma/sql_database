package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.NewComment;
import dao.NewCommentDao;
import dao.UserCommentDao;


public class NewCommentService {

    public void register(NewComment new_comment) {

        Connection connection = null;
        try {
            connection = getConnection();



            NewCommentDao userDao = new NewCommentDao();
            userDao.insert(connection, new_comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

private static final int LIMIT_NUM = 1000;

public List<NewComment>getComment(){
	Connection connection = null;
	try{
		connection = getConnection();

		UserCommentDao commentDao = new UserCommentDao();
		List<NewComment> ret = commentDao.getUserComments(connection,LIMIT_NUM);
		commit(connection);
		return ret;
	}catch(RuntimeException e){
		rollback(connection);
		throw e;
	}catch(Error e){
		rollback(connection);
		throw e;
	}finally{
		close(connection);
	}
}


}