package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.NewComment;
import exception.SQLRuntimeException;

public class NewCommentDao {

	public void insert(Connection connection, NewComment new_comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");

			sql.append(" text");
			sql.append(", user_id");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(", message_id");
			sql.append(") VALUES (");

			sql.append(" ?"); // text
			sql.append(", ?"); // user_id
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", ?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, new_comment.getText());
			ps.setInt(2, new_comment.getUser_id());
			ps.setInt(3, new_comment.getMessage_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}