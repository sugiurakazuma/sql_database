package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Delete;
import beans.NewMessage;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, NewMessage new_message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributiones ( ");
			sql.append("subject");
			sql.append(", category");
			sql.append(", text");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(", user_id");
			sql.append(") VALUES (");
			sql.append("?"); // subject
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", ?");// user_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, new_message.getSubject());
			ps.setString(2, new_message.getCategory());
			ps.setString(3, new_message.getText());
			ps.setInt(4, new_message.getUser_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void insert(Connection connection, Delete delete_message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM contributiones WHERE id;  ");

			ps = connection.prepareStatement(sql.toString());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}