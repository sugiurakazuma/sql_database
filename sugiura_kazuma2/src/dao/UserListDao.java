package dao;

import static utils.CloseableUtil.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Users;

public class UserListDao {

	private List<Users> toUsers(ResultSet rs) throws SQLException {

		List<Users> ret = new ArrayList<Users>();
		try {
			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				String login_id = rs.getString("login_id");
				String branch_id = rs.getString("branch_id");
				String position_id = rs.getString("position_id");

				Users message = new Users(id, name, login_id, branch_id, position_id);
				message.setId(id);
				message.setName(name);
				message.setLogin_id(login_id);
				message.setBranch_id(branch_id);
				message.setPosition_id(position_id);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}