package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User users) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("name");
			sql.append(",login_id");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", 0");

			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, users.getName());
			ps.setString(2, users.getLogin_id());
			ps.setString(3, users.getPassword());
			ps.setString(4, users.getBranch_id());
			ps.setString(5, users.getPosition_id());
			ps.setInt(6, users.getIs_deleted());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String login_id, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT *  from users where login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, login_id);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String branch_id = rs.getString("branch_id");
				String position_id = rs.getString("position_id");
				int is_deleted = rs.getInt("is_deleted");
				User users = new User();
				users.setId(id);
				users.setName(name);
				users.setLogin_id(login_id);
				users.setPassword(password);
				users.setBranch_id(branch_id);
				users.setPosition_id(position_id);
				users.setIs_deleted(is_deleted);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getUserList(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM sugiura_kazuma.users";

			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			return userList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE  users SET  ");
			sql.append("login_id = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getBranch_id());
			ps.setString(5, user.getPosition_id());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}